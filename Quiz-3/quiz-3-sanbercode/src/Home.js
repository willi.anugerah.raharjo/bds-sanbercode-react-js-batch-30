import logo from './logo.png';
import logo2 from './logo2.jpg';
import './App.css';
import React from "react"

const Home = () => {

    return (
        <>
        <div className="row">
        <div className="section">
        <h1 style={{textAlign: "center"}}>Popular Mobile Apps</h1>
            <div className="card">
                <div>
                    <h2>Mobile Legends</h2>
                    <h5>Release Year : 2007</h5>
                    <img className="fakeimg" src={logo2} style={{width: "50%",height: "300px",objectFit: "cover"}} />
                    <br />
                    <br />
                    <div>
                        <b>Price: Free</b><br />
                        <b>Rating: 4.3</b><br />
                        <b>Size: 75Mb</b><br />
                        <b style={{marginRright: "10px"}}>Platform : Android & IOS
                        </b>
                        <br />
                    </div>
                    <p>
                        <b style={{marginRright: "10px"}}>Description :</b>
                        {/* {item.description} */}
                    </p>

                    <hr />
                </div>
            </div>

        </div>
    </div>
    <footer>
        <h5>&copy; Quiz 3 ReactJS Sanbercode</h5>
    </footer>
    </>
    );
  };


export default Home;
