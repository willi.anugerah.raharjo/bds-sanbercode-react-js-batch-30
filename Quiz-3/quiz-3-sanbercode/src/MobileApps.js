import React from "react"
import {MobileProvider} from "./MobileContext"
import MobileList from "./MobileList"
import MobileForm from "./MobileForm"

const Mobile = () =>{
  return(
    <MobileProvider>
      <MobileList/>
      <MobileForm/>
    </MobileProvider>
  )
}

export default Mobile