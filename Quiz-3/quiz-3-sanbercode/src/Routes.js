import logo from './logo.png';
import './App.css';
import React from "react"
import { Switch, Route, BrowserRouter as Router, Link } from "react-router-dom";
import About from "./About"
import Dashboard from "./Home"
import Mobile from './MobileApps';
import MobileForm from './MobileForm';

const Home = () => {

    return (
      <Router>
        <Nav/>
        <Switch>
          <Route exact path="/">
            <Dashboard />
          </Route>
          <Route path="/movie-list">
            <Mobile />
          </Route>
          <Route exact path="/about">
            <About />
          </Route>
          <Route exact path="/mobile-form">
            <MobileForm />
          </Route>
        </Switch>
      </Router>
    );
  };


const Nav = () => {

  return (
      <>
    <div className="topnav">
        <a href="">
            <img src={logo} width="70" alt="logo" />
        </a>
        <Link to="/">Home</Link>
        <Link to="/movie-list">Movie List</Link>
        <Link to="/about">About</Link>
        {/* <Link to="/mobile-form">Form</Link> */}
        <form>
            <input type="text" />
            <input type="submit" value="Cari" />
        </form>
    </div>

    </>
  );
}

export default Home;
