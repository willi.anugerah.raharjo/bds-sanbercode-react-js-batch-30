import React, {useContext, useEffect, useState} from "react"
import {MobileContext} from "./MobileContext"
import axios from "axios"
import { useHistory } from "react-router-dom";

const MobileList = () =>{
    const {daftarMobile, setdaftarMobile} = useContext(MobileContext)
    // Komen dibwah ini klo dibuka kenapa errrrorrrr yaaaaaaa
    // const [inputName, setInputName] =  useContext(MahasiswaContext)
    // const [inputCategory, setInputCategory] =  useContext(MobileContext)
    // const [inputReleaseYear, setinputReleaseYear] =  useContext(MobileContext)
    // const [inputSize, setinputSize] =  useContext(MobileContext)
    // const [inputPrice, setinputPrice] =  useContext(MobileContext)
    // const [inputRating, setinputRating] =  useContext(MobileContext)
    // const [inputImageUrl, setinputImageUrl] =  useContext(MobileContext)
    // const [inputAndroid, setinputAndroid] =  useContext(MobileContext)
    // const [inputIos, setinputIos] =  useContext(MobileContext)
    const [currentId, setCurrentId] =  useState(null)

  let history = useHistory();

  useEffect( () => {
    const fetchData = async () => {
      const result = await axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps`)

      setdaftarMobile(result.data.map(x=>{ return {
         id: x.id, name: x.name,
         description: x.description, category:x.category,
         size:x.size, price:x.price, rating:x.rating,
         image_url:x.image_url, release_year:x.release_year,
         is_android_app:x.is_android_app, is_ios_app:x.is_ios_app} }) )
    }
      
    fetchData()
  }, [])

  const handleEdit = (event) =>{
        let idMobile = event.target.value
        axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps/${idMobile}`)
        .then(res => {
          let data = res.data
          // Komen dibwah ini klo dibuka kenapa errrrorrrr yaaaaaaa
        //   setInputName(data.name)
        //   setInputCategory(data.category)
        //   setinputReleaseYear(data.release_year)
        //   setinputSize(data.size)
        //   setinputPrice(data.price)
        //   setinputRating(data.rating)
        //   setinputImageUrl(data.image_url)
        //   setinputAndroid(data.is_android_app)
        //   setinputIos(data.is_ios_app)
        //   setCurrentId(data.id)
        })
      }
    
      const handleDelete = (event) =>{
        let idMobile = parseInt(event.target.value)
        axios.delete(`http://backendexample.sanbercloud.com/api/mobile-apps/${idMobile}`)
        .then(() => {
          let newdaftarMobile = daftarMobile.filter(el=> {return el.id !== idMobile})
          setdaftarMobile(newdaftarMobile)
        })
      }

    function handleInsert() {
      history.push("/mobile-form");
    }

  return(
    <>
      { daftarMobile !== null &&
        (<div className="row">
        <div className="section">
          <h1 style={{textAlign:"center"}}>Daftar Nilai Mobile</h1>
          <button onClick={handleInsert}>Tambah Data</button>
          <br/><br/>
          <table className="daftar-Mobile" border="1">
            <thead>
              <tr>
                <th>No</th>
                <th style={{width:"150px", textAlign:"center"}}>Nama</th>
                <th style={{width:"150px", textAlign:"center"}}>Kategori</th>
                <th style={{width:"50px", textAlign:"center"}}>Deskripsi</th>
                <th style={{width:"70px", textAlign:"center"}}>Tahun Release</th>
                <th style={{width:"70px", textAlign:"center"}}>Size</th>
                <th style={{width:"70px", textAlign:"center"}}>Harga</th>
                <th style={{width:"70px", textAlign:"center"}}>Rating</th>
                <th style={{width:"70px", textAlign:"center"}}>Platform</th>
                <th style={{textAlign:"center"}}>Action</th>
              </tr>
            </thead>
            <tbody>
                {
                  daftarMobile.map((item, index)=>{
                    let platform_a = ""
                    if (item.is_android_app != null || item.is_android_app == 1) {
                        platform_a = "Android"
                    }else{
                        platform_a = ""
                    }

                    let platform_b = ""
                    if (item.is_ios_app != null || item.is_ios_app == 1) {
                        platform_b = "IOS"
                    }else{
                        platform_b = ""
                    }

                    let harga = ""
                    if (item.price == 0){
                        harga = "FREE"
                    }else{
                        harga = item.price
                    }

                    return(                    
                      <tr key={index}>
                        <td>{index+1}</td>
                        <td>{item.name}</td>
                        <td>{item.category}</td>
                        <td style={{textAlign:"center"}}>{item.description}</td>
                        <td style={{textAlign:"center"}}>{item.release_year}</td>
                        <td style={{textAlign:"center"}}>{item.size / 1000} GB</td>
                        <td style={{textAlign:"center"}}>{harga}</td>
                        <td style={{textAlign:"center"}}>{item.rating}</td>
                        <td style={{textAlign:"center"}}>{platform_a} <br/> {platform_b}</td>
                        <td>
                        <button onClick={handleEdit} value={item.id}>Edit</button>
                        &nbsp;
                        <button onClick={handleDelete} value={item.id}>Delete</button>
                        </td>
                      </tr>
                    )
                  })
                }
            </tbody>
          </table>

          <br/>
          <br/>

        </div>
        </div>
        )
      }

    </>
  )
}

export default MobileList