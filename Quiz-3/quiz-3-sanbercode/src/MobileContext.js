import React, { useState, createContext } from "react";

export const MobileContext = createContext();

 export const MobileProvider = props =>{
    const [daftarMobile, setdaftarMobile] =  useState([])
    const [inputName, setInputName] =  useState("")
    const [inputCategory, setInputCategory] =  useState("")
    const [inputReleaseYear, setinputReleaseYear] =  useState("")
    const [inputSize, setinputSize] =  useState("")
    const [inputPrice, setinputPrice] =  useState("")
    const [inputRating, setinputRating] =  useState("")
    const [inputImageUrl, setinputImageUrl] =  useState("")
    const [inputAndroid, setinputAndroid] =  useState("")
    const [inputIos, setinputIos] =  useState("")
    

  return (
    <MobileContext.Provider value={{
        daftarMobile, 
        setdaftarMobile,
        inputName,
        setInputName,
        inputCategory,
        setInputCategory,
        inputReleaseYear,
        setinputReleaseYear,
        inputSize,
        setinputSize,
        inputPrice,
        setinputPrice,
        inputRating,
        setinputRating,
        inputImageUrl,
        setinputImageUrl,
        inputAndroid,
        setinputAndroid,
        inputIos,
        setinputIos
    }}>
      {props.children}
    </MobileContext.Provider>
  );
};