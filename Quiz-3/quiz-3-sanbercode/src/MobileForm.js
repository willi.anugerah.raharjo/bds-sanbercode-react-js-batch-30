import React, { useContext, useState } from "react"
import {MobileContext} from "./MobileContext"
import axios from "axios"

const MobileForm = () => {

    const {daftarMobile, setdaftarMobile} = useContext(MobileContext)
    const [inputName, setInputName] =  useContext(MobileContext)
    const [inputCategory, setInputCategory] =  useContext(MobileContext)
    const [inputReleaseYear, setinputReleaseYear] =  useContext(MobileContext)
    const [inputSize, setinputSize] =  useContext(MobileContext)
    const [inputPrice, setinputPrice] =  useContext(MobileContext)
    const [inputRating, setinputRating] =  useContext(MobileContext)
    const [inputImageUrl, setinputImageUrl] =  useContext(MobileContext)
    const [inputAndroid, setinputAndroid] =  useContext(MobileContext)
    const [inputIos, setinputIos] =  useContext(MobileContext)
    const [currentId, setCurrentId] =  useState(null)
    
      const handleChange = (event) =>{
        let inputValue = event.target.value
        setInputName(inputValue)
      }
    
      const handleChangeCategory = (event) =>{
        let inputValue = event.target.value
        setInputCategory(inputValue)
      }
    
      const handleChangeReleaseYear = (event) =>{
        let inputValue = event.target.value
        setinputReleaseYear(inputValue)
      }

      const handleChangeSize = (event) =>{
        let inputValue = event.target.value
        setinputSize(inputValue)
      }

      const handleChangePrice = (event) =>{
        let inputValue = event.target.value
        setinputPrice(inputValue)
      }

      const handleChangeRating = (event) =>{
        let inputValue = event.target.value
        setinputRating(inputValue)
      }

      const handleChangeImageUrl = (event) =>{
        let inputValue = event.target.value
        setinputImageUrl(inputValue)
      }

      const handleChangeAndroid = (event) =>{
        let inputValue = event.target.value
        setinputAndroid(inputValue)
      }

      const handleChangeIos = (event) =>{
        let inputValue = event.target.value
        setinputIos(inputValue)
      }
    
      const handleSubmit = (event) =>{
        event.preventDefault()
    
        if (currentId === null){
          // untuk create data baru
          axios.post(`http://backendexample.sanbercloud.com/api/mobile-apps`, {
              name: inputName, category: inputCategory, release_year: inputReleaseYear,
            size: inputSize, price: inputPrice, rating: inputRating,
        image_url: inputImageUrl, is_android_app: inputAndroid, is_ios_app: inputIos})
          .then(res => {
              let data = res.data
              setdaftarMobile([...daftarMobile, {
                  id: data.id, name: data.name, category: data.category,
                  release_year: data.release_year, size: data.size, price: data.price,
                rating: data.rating, image_url: data.image_url, is_android_app: data.is_android_app,
            is_ios_app: data.is_ios_app}])
          })
        }else{
          axios.put(`http://backendexample.sanbercloud.com/api/mobile-apps/${currentId}`, {
            name: inputName, category: inputCategory, release_year: inputReleaseYear,
            size: inputSize, price: inputPrice, rating: inputRating,
        image_url: inputImageUrl, is_android_app: inputAndroid, is_ios_app: inputIos})
          .then(() => {
              let singlePeserta = daftarMobile.find(el=> el.id === currentId)
              singlePeserta.name= inputName
              singlePeserta.category = inputCategory
              singlePeserta.release_year = inputReleaseYear
              singlePeserta.size = inputSize
              singlePeserta.price = inputPrice
              singlePeserta.rating = inputRating
              singlePeserta.image_url = inputImageUrl
              singlePeserta.is_android_app = inputAndroid
              singlePeserta.is_ios_app = inputIos
              setdaftarMobile([...daftarMobile])
          })      
        }
        setInputName("")
        setInputCategory("")
        setinputReleaseYear("")
        setinputSize("")
        setinputPrice("")
        setinputRating("")
        setinputImageUrl("")
        setinputAndroid("")
        setinputIos("")
        setCurrentId(null)
      }

    return(
        <>
        <div className="row">
        <div className="section"></div>
        <div style={{width: "90%", margin: "0 auto", textAlign: "justify"}}>
        {/* Form */}
        <h1>Form Tambah Mobile</h1>
        <form style={{paddingBottom: "20px"}}onSubmit={handleSubmit}>
        <label>
            Name:
        </label>          
        <input style={{marginLeft:"48px"}} type="text" value={inputName} onChange={handleChange}/>
        <br/>
        <label>
            Category:
        </label>          
        <input style={{marginLeft:"12px"}} type="text" value={inputCategory} onChange={handleChangeCategory}/>
        <br/>
        <label>
            Release Year:
        </label>          
        <input style={{marginLeft:"58px"}} type="text" value={inputReleaseYear} onChange={handleChangeReleaseYear}/>
        <br/>
        <label>
            Size:
        </label>          
        <input style={{marginLeft:"58px"}} type="text" value={inputSize} onChange={handleChangeSize}/>
        <br/>
        <label>
            Price:
        </label>          
        <input style={{marginLeft:"58px"}} type="text" value={inputPrice} onChange={handleChangePrice}/>
        <br/>
        <label>
            Rating:
        </label>          
        <input style={{marginLeft:"58px"}} type="text" value={inputRating} onChange={handleChangeRating}/>
        <br/>
        <label>
            Image URL:
        </label>          
        <input style={{marginLeft:"58px"}} type="text" value={inputImageUrl} onChange={handleChangeImageUrl}/>
        <br/>
        <label>
            Is Android:
        </label>          
        <input style={{marginLeft:"58px"}} type="text" value={inputAndroid} onChange={handleChangeAndroid}/>
        <br/>
        <label>
            Is IOS:
        </label>          
        <input style={{marginLeft:"58px"}} type="text" value={inputIos} onChange={handleChangeIos}/>
        <br/>
        <br/>
        <button style={{marginLeft:"209px"}}>submit</button>
        </form>
        </div>
        </div>
        </>
    )
}

export default MobileForm