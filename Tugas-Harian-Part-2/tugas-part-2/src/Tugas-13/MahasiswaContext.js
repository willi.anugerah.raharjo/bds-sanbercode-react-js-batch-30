import React, { useState, createContext } from "react";

export const MahasiswaContext = createContext();

 export const MahasiswaProvider = props =>{
    const [daftarNilaiMahasiswa, setdaftarNilaiMahasiswa] =  useState([])
    const [inputName, setInputName] =  useState("")
    const [inputNameCourse, setInputNameCourse] =  useState("")
    const [inputCourseScore, setInputCourseScore] =  useState("")
    

  return (
    <MahasiswaContext.Provider value={{
        daftarNilaiMahasiswa, 
        setdaftarNilaiMahasiswa,
        inputName,
        setInputName,
        inputNameCourse,
        setInputNameCourse,
        inputCourseScore,
        setInputCourseScore
    }}>
      {props.children}
    </MahasiswaContext.Provider>
  );
};