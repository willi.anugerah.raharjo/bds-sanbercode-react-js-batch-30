import logo from './logo.png';
import './App.css';

function Tugas9() {
  const ColoredLine = ({ color }) => (
    <hr
        style={{
            color: color,
            backgroundColor: color,
            height: "2px",
            width: "380px"
        }}
    />
);

const ColoredLine2 = ({ color }) => (
  <hr
      style={{
          color: color,
          backgroundColor: color,
          height: "0.3px",
          width: "380px"
      }}
  />
);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p class="judul">THINGS TO DO</p>
       
          During bootcamp in sanbercode.
          <ColoredLine color="grey" />
          
        <form className="label">
          <label className="label">
            <input className="cekbox" type="checkbox" name="1" />
            &nbsp; Belajar GIT & CLI
          </label>
          <ColoredLine2 color="grey" />
          <p></p>
          <label className="label">
            <input className="cekbox" type="checkbox" name="2" />
            &nbsp; Belajar HTML & CSS
          </label>
          <ColoredLine2 color="grey" />
          <p></p>
          <label className="label">
            <input className="cekbox" type="checkbox" name="3" />
            &nbsp; Belajar Javascript
          </label>
          <ColoredLine2 color="grey" />
          <p></p>
          <label className="label">
            <input className="cekbox" type="checkbox" name="4" />
            &nbsp; Belajar React JS Dasar
          </label>
          <ColoredLine2 color="grey" />
          <p></p>
          <label className="label">
            <input className="cekbox" type="checkbox" name="2" />
            &nbsp; Belajar React JS Advance
          </label>
          <ColoredLine2 color="grey" />
          <p></p>
          <input className="btn" type="button" value="Send" />
        </form>
      </header>
    </div>
  );
}

export default Tugas9;
