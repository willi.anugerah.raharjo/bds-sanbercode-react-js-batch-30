import React, {useState, useEffect} from "react"
import axios from "axios"

const Mahasiswa= () =>{
  const [daftarNilaiMahasiswa, setdaftarNilaiMahasiswa] =  useState([])
  const [inputName, setInputName] =  useState("")
  const [inputNameCourse, setInputNameCourse] =  useState("")
  const [inputCourseScore, setInputCourseScore] =  useState("")
  const [currentId, setCurrentId] =  useState(null)

  useEffect( () => {
    const fetchData = async () => {
      const result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)

      setdaftarNilaiMahasiswa(result.data.map(x=>{ return {id: x.id, name: x.name, course:x.course, score:x.score} }) )
    }
      
    fetchData()
  }, [])

  const handleEdit = (event) =>{
    let idPeserta = event.target.value
    axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${idPeserta}`)
    .then(res => {
      let data = res.data
      setInputName(data.name)
      setInputNameCourse(data.course)
      setInputCourseScore(data.score)
      setCurrentId(data.id)
    })
  }

  const handleDelete = (event) =>{
    let idPeserta = parseInt(event.target.value)
    axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${idPeserta}`)
    .then(() => {
      let newdaftarNilaiMahasiswa = daftarNilaiMahasiswa.filter(el=> {return el.id !== idPeserta})
      setdaftarNilaiMahasiswa(newdaftarNilaiMahasiswa)
    })
  }

  const handleChange = (event) =>{
    let inputValue = event.target.value
    setInputName(inputValue)
  }

  const handleChangeCourse = (event) =>{
    let inputValue = event.target.value
    setInputNameCourse(inputValue)
  }

  const handleChangeScore = (event) =>{
    let inputValue = event.target.value
    setInputCourseScore(inputValue)
  }

  const handleSubmit = (event) =>{
    event.preventDefault()

    if (currentId === null){
      // untuk create data baru
      axios.post(`http://backendexample.sanbercloud.com/api/student-scores`, {name: inputName, course: inputNameCourse, score: inputCourseScore})
      .then(res => {
          let data = res.data
          setdaftarNilaiMahasiswa([...daftarNilaiMahasiswa, {id: data.id, name: data.name, course: data.course, score: data.score}])
      })
    }else{
      axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentId}`, {name: inputName, course: inputNameCourse, score: inputCourseScore})
      .then(() => {
          let singlePeserta = daftarNilaiMahasiswa.find(el=> el.id === currentId)
          singlePeserta.name= inputName
          singlePeserta.course = inputNameCourse
          singlePeserta.score = inputCourseScore
          setdaftarNilaiMahasiswa([...daftarNilaiMahasiswa])
      })      
    }
    setInputName("")
    setInputNameCourse("")
    setInputCourseScore("")
    setCurrentId(null)
  }

  return(
    <>
      { daftarNilaiMahasiswa !== null &&
        (<div style={{width: "90%", margin: "0 auto", textAlign: "justify"}}>
          <h1>Daftar Nilai Mahasiswa</h1>
          <table className="daftar-mahasiswa" border="1">
            <thead>
              <tr>
                <th>No</th>
                <th style={{width:"150px", textAlign:"center"}}>Nama</th>
                <th style={{width:"150px", textAlign:"center"}}>Matakuliah</th>
                <th style={{width:"50px", textAlign:"center"}}>Nilai</th>
                <th style={{width:"70px", textAlign:"center"}}>Indeks Nilai</th>
                <th style={{textAlign:"center"}}>Action</th>
              </tr>
            </thead>
            <tbody>
                {
                  daftarNilaiMahasiswa.map((item, index)=>{
                    let indeks = ""
                    if (item.score >= 80) {
                        indeks = "A"
                    }else if (item.score >= 70 && item.score < 80){
                        indeks = "B"
                    }else if (item.score >= 60 && item.score < 70){
                        indeks = "C"
                    }else if (item.score >= 50 && item.score < 60){
                        indeks = "D"
                    }else{
                        indeks = "E"
                    }

                    return(                    
                      <tr key={index}>
                        <td>{index+1}</td>
                        <td>{item.name}</td>
                        <td>{item.course}</td>
                        <td style={{textAlign:"center"}}>{item.score}</td>
                        <td style={{textAlign:"center"}}>{indeks}</td>
                        <td>
                        <button onClick={handleEdit} value={item.id}>Edit</button>
                        &nbsp;
                        <button onClick={handleDelete} value={item.id}>Delete</button>
                        </td>
                      </tr>
                    )
                  })
                }
            </tbody>
          </table>

          <br/>
          <br/>
          {/* Form */}
            <h1>Form Nilai Mahasiswa</h1>
            <form style={{paddingBottom: "20px"}}onSubmit={handleSubmit}>
            <label>
                Nama:
            </label>          
            <input style={{marginLeft:"48px"}} type="text" value={inputName} onChange={handleChange}/>
            <br/>
            <label>
                Matakuliah:
            </label>          
            <input style={{marginLeft:"12px"}} type="text" value={inputNameCourse} onChange={handleChangeCourse}/>
            <br/>
            <label>
                Nilai:
            </label>          
            <input style={{marginLeft:"58px"}} type="text" value={inputCourseScore} onChange={handleChangeScore}/>
            <br/>
            <br/>
            <button style={{marginLeft:"209px"}}>submit</button>
            </form>

        </div>)
      }

    </>
  )
}

export default Mahasiswa