import React from "react";
import Tugas9 from "./Tugas9";
import Tugas11 from "./Tugas11";
import Tugas12 from "./Tugas12";
import Tugas13 from "./Mahasiswa";
import MahasiswaForm from "./MahasiswaForm"
import { Switch, Route, BrowserRouter as Router, Link } from "react-router-dom";

const Routes = () => {

  return (
    <Router>
      <Nav/>
      <Switch>
        <Route exact path="/">
          <Tugas9 />
        </Route>
        <Route path="/tugas10">
          {/* <About /> */}
        </Route>
        <Route exact path="/tugas11">
          <Tugas11 />
        </Route>
        <Route exact path="/tugas12">
          <Tugas12 />
        </Route>
        <Route exact path="/tugas13">
          <Tugas13 />
        </Route>
        <Route exact path="/mahasiswaform">
          <MahasiswaForm />
        </Route>
      </Switch>
    </Router>
  );
};

const Nav = () => {
    return (
      <>
      <div className="header">
        <ul className="horizontal">
          <li>
            <Link to="/">Tugas 9</Link>
          </li>
          <li>
            <Link to="/tugas10">Tugas 10</Link>
          </li>
          <li>
            <Link to="/tugas11">Tugas 11</Link>
          </li>
          <li>
            <Link to="/tugas12">Tugas 12</Link>
          </li>
          <li>
            <Link to="/tugas13">Tugas 13</Link>
          </li>
        </ul>
        </div>
      </>
    )
  }

export default Routes;