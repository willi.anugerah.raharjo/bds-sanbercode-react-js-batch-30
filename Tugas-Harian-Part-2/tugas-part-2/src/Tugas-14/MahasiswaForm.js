import React, { useContext, useState } from "react"
import { MahasiswaContext } from "./MahasiswaContext"
import axios from "axios"

const MahasiswaForm = () => {

    const {daftarNilaiMahasiswa, setdaftarNilaiMahasiswa} = useContext(MahasiswaContext)
    const {inputName, setInputName} =  useContext(MahasiswaContext)
    const {inputNameCourse, setInputNameCourse} =  useContext(MahasiswaContext)
    const {inputCourseScore, setInputCourseScore} =  useContext(MahasiswaContext)
    const [currentId, setCurrentId] =  useState(null)
    
      const handleChange = (event) =>{
        let inputValue = event.target.value
        setInputName(inputValue)
      }
    
      const handleChangeCourse = (event) =>{
        let inputValue = event.target.value
        setInputNameCourse(inputValue)
      }
    
      const handleChangeScore = (event) =>{
        let inputValue = event.target.value
        setInputCourseScore(inputValue)
      }
    
      const handleSubmit = (event) =>{
        event.preventDefault()
    
        if (currentId === null){
          // untuk create data baru
          axios.post(`http://backendexample.sanbercloud.com/api/student-scores`, {name: inputName, course: inputNameCourse, score: inputCourseScore})
          .then(res => {
              let data = res.data
              setdaftarNilaiMahasiswa([...daftarNilaiMahasiswa, {id: data.id, name: data.name, course: data.course, score: data.score}])
          })
        }else{
          axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentId}`, {name: inputName, course: inputNameCourse, score: inputCourseScore})
          .then(() => {
              let singlePeserta = daftarNilaiMahasiswa.find(el=> el.id === currentId)
              singlePeserta.name= inputName
              singlePeserta.course = inputNameCourse
              singlePeserta.score = inputCourseScore
              setdaftarNilaiMahasiswa([...daftarNilaiMahasiswa])
          })      
        }
        setInputName("")
        setInputNameCourse("")
        setInputCourseScore("")
        setCurrentId(null)
      }

    return(
        <>
        <div style={{width: "90%", margin: "0 auto", textAlign: "justify"}}>
        {/* Form */}
        <h1>Form Nilai Mahasiswa</h1>
        <form style={{paddingBottom: "20px"}}onSubmit={handleSubmit}>
        <label>
            Nama:
        </label>          
        <input style={{marginLeft:"48px"}} type="text" value={inputName} onChange={handleChange}/>
        <br/>
        <label>
            Matakuliah:
        </label>          
        <input style={{marginLeft:"12px"}} type="text" value={inputNameCourse} onChange={handleChangeCourse}/>
        <br/>
        <label>
            Nilai:
        </label>          
        <input style={{marginLeft:"58px"}} type="text" value={inputCourseScore} onChange={handleChangeScore}/>
        <br/>
        <br/>
        <button style={{marginLeft:"209px"}}>submit</button>
        </form>
        </div>
        </>
    )
}

export default MahasiswaForm