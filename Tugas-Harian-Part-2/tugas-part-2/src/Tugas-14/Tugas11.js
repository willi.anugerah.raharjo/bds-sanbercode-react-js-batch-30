import React, { useState } from 'react';

const Tugas11 = () => {
  const [daftarBuah, setdaftarBuah] = useState(
    [
        {nama: "Nanas", hargaTotal: 100000, beratTotal: 4000 },
        {nama: "Manggis", hargaTotal: 350000, beratTotal: 10000},
        {nama: "Nangka", hargaTotal: 90000, beratTotal: 2000},
        {nama: "Durian", hargaTotal: 400000, beratTotal: 5000},
        {nama: "Strawberry", hargaTotal: 120000, beratTotal: 6000}
    ]
  )

  const [buahName, setBuahName] = useState("")
  const [buahHargaTotal, setBuahHargaTotal] = useState("")
  const [buahBeratTotal, setBuahBeratTotal] = useState("")
  const [currentIndex, setCurrentIndex] = useState(-1)

  const handleChangeBuah = (event) => {
    setBuahName(event.target.value );
  }

  const handleChangeBuahHarga = (event) => {
    setBuahHargaTotal(event.target.value );
  }

  const handleChangeBuahBerat = (event) => {
    setBuahBeratTotal(event.target.value );
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    let newData = daftarBuah
    let arr = 
        {
            nama: buahName,
            hargaTotal: buahHargaTotal,
            beratTotal: buahBeratTotal
        }

        if (currentIndex === -1) {
            newData = [...daftarBuah, arr]
          } else {
            newData[currentIndex] = arr
        }
    
    setdaftarBuah(newData)
    setBuahName("")
    setBuahHargaTotal("")
    setBuahBeratTotal("")
  }

  const handleDelete = (event) => {
    let index = parseInt(event.target.value)
    let deletedItem = daftarBuah[index]
    let newData = daftarBuah.filter((e) => {return e !== deletedItem})
    setdaftarBuah(newData)
  }

  const handleEdit = (event) => {
    let index = parseInt(event.target.value)
    let editValueName = daftarBuah[index].nama
    let editValueHarga = daftarBuah[index].hargaTotal
    let editValueBerat = daftarBuah[index].beratTotal
    setBuahName(editValueName)
    setBuahHargaTotal(editValueHarga)
    setBuahBeratTotal(editValueBerat)
    setCurrentIndex(event.target.value)
}

  return (
    <>
    <br/>
    <form onSubmit={handleSubmit}>
            <label>
                Nama:
                <input style={{marginLeft:"150px"}} type="text" id="nama" value={buahName} onChange={handleChangeBuah} />
            </label>
            <br/>
            <label>
                Harga Total:
                <input style={{marginLeft:"111px"}} type="text" id="harga_total" value={buahHargaTotal} onChange={handleChangeBuahHarga} />
            </label>
            <br/>
            <label>
                Berat Total (dalam gram):
                <input style={{marginLeft:"18px"}} type="text" id="berat_total" value={buahBeratTotal} onChange={handleChangeBuahBerat} />
            </label>
            <br/>
            <br/>
            <button style={{marginLeft:"310px"}}>submit</button>
            </form>
    <br/>
    <br/>
      <h1>Daftar Buah</h1>
      <table border="1">
        <thead>
          <tr>
            <th style={{width:"40px"}}>No</th>
            <th style={{width:"100px"}}>Nama</th>
            <th style={{width:"100px"}}>Harga Total</th>
            <th style={{width:"100px"}}>Berat Total</th>
            <th style={{width:"100px"}}>Harga per Kg</th>
            <th style={{width:"130px"}}>Aksi</th>
          </tr>
        </thead>
        <tbody>
          {
            daftarBuah.map((val, index) => {
              return (
                <tr>
                  <td>{index + 1}</td>
                  <td>{val.nama}</td>
                  <td>{val.hargaTotal}</td>
                  <td>{val.beratTotal / 1000 + "kg"}</td>
                  <td>{val.hargaTotal / (val.beratTotal / 1000)}</td>
                  <td style={{align:"center"}}><button onClick={handleEdit} value={index}>edit</button>
                  <button onClick={handleDelete} value={index}>delete</button></td>
                </tr>
              )
            })
          }

        </tbody>
      </table>
    </>
  )

}

export default Tugas11