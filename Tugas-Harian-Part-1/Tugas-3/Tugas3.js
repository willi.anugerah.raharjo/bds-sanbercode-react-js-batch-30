//soal 1

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

console.log("//soal 1")
console.log(kataPertama+" "+kataKedua.charAt(0).toUpperCase()+kataKedua.substr(1,5)+" "+kataKetiga.substr(0,6)+kataKetiga.charAt(6).toUpperCase()+" "+kataKeempat.toUpperCase());

//soal 2

var panjangPersegiPanjang = "8";
var lebarPersegiPanjang = "5";

var alasSegitiga= "6";
var tinggiSegitiga = "7";

var kelilingPersegiPanjang = 0;
var luasSegitiga = 0;

kelilingPersegiPanjang = 2 * (parseInt(panjangPersegiPanjang) + parseInt(lebarPersegiPanjang));
luasSegitiga = parseInt(alasSegitiga) + parseInt(tinggiSegitiga) / 2;

console.log("//soal 2")
console.log(kelilingPersegiPanjang);
console.log(luasSegitiga);

//soal 3
var sentences= 'wah javascript itu keren sekali'; 

var firstWord= sentences.substring(0, 3); 
var secondWord = sentences.substring(4,14); // do your own! 
var thirdWord = sentences.substring(15, 18); // do your own! 
var fourthWord = sentences.substring(19, 25); // do your own! 
var fifthWord = sentences.substring(25, 32); // do your own! 

console.log("//soal 3")
console.log('Kata Pertama: ' + firstWord); 
console.log('Kata Kedua: ' + secondWord); 
console.log('Kata Ketiga: ' + thirdWord); 
console.log('Kata Keempat: ' + fourthWord); 
console.log('Kata Kelima: ' + fifthWord);

//soal 4
var nilaiJohn = 80;
var nilaiDoe = 50;
var indeksJohn = "";

if (nilaiJohn >= 80){
    indeksJohn = "A";
}else if ((nilaiJohn >= 70) && (nilaiJohn < 80)){
    indeksJohn = "B";
}else if ((nilaiJohn >= 60) && (nilaiJohn < 70)){
    indeksJohn = "C";
}else if ((nilaiJohn >= 50) && (nilaiJohn < 60)){
    indeksJohn = "D";
}else{
    indeksJohn = "E";
}

if (nilaiDoe >= 80){
    indeksDoe = "A";
}else if ((nilaiDoe >= 70) && (nilaiDoe < 80)){
    indeksDoe = "B";
}else if ((nilaiDoe >= 60) && (nilaiDoe < 70)){
    indeksDoe = "C";
}else if ((nilaiDoe >= 50) && (nilaiDoe < 60)){
    indeksDoe = "D";
}else{
    indeksDoe = "E";
}

console.log("//soal 4");
console.log(indeksJohn);
console.log(indeksDoe);

//soal 5
var tanggal = 20;
var bulan = 11;
var tahun = 1993;
var nama_bulan = "";

// ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda 
// dan buatlah switch case pada bulan, lalu muncul kan string nya dengan output seperti ini 22 Juli 2020
//  (isi di sesuaikan dengan tanggal lahir masing-masing)

switch(bulan) {
  case 1:   { nama_bulan = "Januari"; break; }
  case 2:   { nama_bulan = "Februari"; break; }
  case 3:   { nama_bulan = "Maret"; break; }
  case 4:   { nama_bulan = "April"; break; }
  case 5:   { nama_bulan = "Mei"; break; }
  case 6:   { nama_bulan = "Juni"; break; }
  case 7:   { nama_bulan = "Juli"; break; }
  case 8:   { nama_bulan = "Agustus"; break; }
  case 9:   { nama_bulan = "September"; break; }
  case 10:   { nama_bulan = "Oktober"; break; }
  case 11:   { nama_bulan = "November"; break; }
  case 12:   { nama_bulan = "Desember"; break; }
  default:  { nama_bulan = "Unknown"; }}

  console.log("//soal 5")
  console.log(tanggal+" "+nama_bulan+" "+tahun)
