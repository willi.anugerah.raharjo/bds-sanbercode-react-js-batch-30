//soal 1
var Animal = class {
    constructor(name){
        this._name = name
        this._legs = 4
        this._cold_blooded = false
    }
}

//Release 0
var sheep = new Animal("shaun");
 
console.log("//soal 1") // "shaun"
console.log("----- release 0 -----")
console.log(sheep._name) // "shaun"
console.log(sheep._legs) // 4
console.log(sheep._cold_blooded) // false
sheep._legs = 3
console.log(sheep._legs)

console.log("")
console.log("")

//Release 1
var Frog = class extends Animal {
    constructor(name){
        super()
        this._name = name
        this._legs = this._legs
        this._cold_blooded = true
    }

    jump() {
        console.log('hop hop')
    }
}

var Ape = class extends Animal {
    constructor(name){
        super()
        this._name = name
        this._legs = 2
        this._cold_blooded = this._cold_blooded
    }

    yell() {
        console.log('Auooo')
    }
}

console.log("----- release 1 -----")
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
sungokong.legs = 2
console.log(sungokong._name)
console.log(sungokong._legs)
console.log(sungokong._cold_blooded)

var kodok = new Frog("buduk")
kodok.jump() // "hop hop"
console.log(kodok._name)
console.log(kodok._legs)
console.log(kodok._cold_blooded)
console.log("")
console.log("")


//soal 2
var Clock = class {
  
    constructor({template}){
        this.template = template
        this.timer
    }
  
    render() {
      let date = new Date();
  
      let hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      let mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      let secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop () {
      clearInterval(this.timer);
    };
  
    start () {
      this.render();
      this.timer = setInterval(() => {this.render()}, 1000);
    };
  
  }
  
  var clock = new Clock({template: 'h:m:s'});
  console.log("//soal 2")
  clock.start(); 