//soal 1
const luasLingkaran = (jariJari) =>{
    return 3.14 * jariJari * jariJari
}

const kelilingLingkaran = (jariJari) =>{
    return 2 * 3.14 * jariJari
}

let jari_jari = 6

console.log("//soal 1")
console.log(luasLingkaran(jari_jari))
console.log(kelilingLingkaran(jari_jari))

//soal 2
const introduce = (nama, umur, jenisKelamin, pekerjaan) =>{
    let data_diri = [nama, umur, jenisKelamin, pekerjaan]

    let [Nama, Umur, JenisKelamin, Pekerjaan] = data_diri;

    let sebutan = ""

    if (JenisKelamin == "Laki-Laki"){
        sebutan = "Pak"
    }else{
        sebutan = "Ibu"
    }

    const theString = `${sebutan} ${Nama} adalah seorang ${Pekerjaan} yang berusia ${Umur} tahun`

    return theString
}

//kode di bawah ini jangan dirubah atau dihapus
const perkenalan = introduce("John", "30", "Laki-Laki", "penulis")
console.log("//soal 2")
console.log(perkenalan) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"

//soal 3
const newFunction = (firstName, lastName) =>{
    return {
      firstName: firstName,
      lastName: lastName,
      fullName : () => {
        console.log(firstName + " " + lastName)
      }
    }
  }
    
  // kode di bawah ini jangan diubah atau dihapus sama sekali
  console.log("//soal 3")
  console.log(newFunction("John", "Doe").firstName)
  console.log(newFunction("Richard", "Roe").lastName)
  newFunction("William", "Imoh").fullName()

  //soal 4
  let phone = {
    name: "Galaxy Note 20",
    brand: "Samsung",
    year: 2020,
    colors: ["Mystic Bronze", "Mystic White", "Mystic Black"]
 }
 // kode diatas ini jangan di rubah atau di hapus sama sekali
 
 /* Tulis kode jawabannya di sini */
const {name, brand, year, colors} = phone
const [colorBronze, colorWhite, colorBlack] = colors

let phoneName = name
let phoneBrand = brand

 // kode di bawah ini jangan dirubah atau dihapus
 console.log("//soal 4")
 console.log(phoneBrand, phoneName, year, colorBlack, colorBronze) 
//  tuliskan kode jawaban yang berisi hasil destructuring yang nantinya akan di gunakan dalam console.log 

//soal 5
let warna = ["biru", "merah", "kuning" , "hijau"]

let dataBukuTambahan= {
  penulis: "john doe",
  tahunTerbit: 2020 
}

let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172,
  warnaSampul:["hitam"]
}
// kode diatas ini jangan di rubah atau di hapus sama sekali

/* Tulis kode jawabannya di sini */
let list_warna = [...buku.warnaSampul, ...warna]
let newBuku = {
    nama : buku.nama,
    jumlahHalaman : buku.jumlahHalaman,
    warnaSampul : list_warna,
    penulis : dataBukuTambahan.penulis,
    tahunTerbit : dataBukuTambahan.tahunTerbit
}

console.log("//soal 5")
console.log(newBuku)