//soal 1

function luasPersegiPanjang(panjang, lebar){
    return panjang * lebar
}

function kelilingPersegiPanjang(panjang, lebar){
    return 2 * (panjang + lebar)
}

function volumeBalok(panjang, lebar, tinggi){
    return panjang * lebar * tinggi
}

var panjang= 12
var lebar= 4
var tinggi = 8
 
var luasPersegiPanjang = luasPersegiPanjang(panjang, lebar)
var kelilingPersegiPanjang = kelilingPersegiPanjang(panjang, lebar)
var volumeBalok = volumeBalok(panjang, lebar, tinggi)

console.log("//soal 1")
console.log(luasPersegiPanjang) 
console.log(kelilingPersegiPanjang)
console.log(volumeBalok)

//soal 2
function introduce(name, age, address, hobby){
   var kalimat = "Nama saya " + name + ", umur saya " + age + " tahun, alamat di " + address + ", dan saya punya hobby yaitu " + hobby + "!"
    return kalimat
}
 
var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log("//soal 2")
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan belum jadi, dan saya punya hobby yaitu Gaming!"

//soal 3
var arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku" , 1992]

var objectDaftarPeserta = {
    Nama : arrayDaftarPeserta[0],
    JenisKelamin : arrayDaftarPeserta[1],
    Hobby : arrayDaftarPeserta[2],
    TTL : arrayDaftarPeserta[3],
}

console.log("//soal 3")
console.log(objectDaftarPeserta)

//soal 4
var objectBuah = [
    {nama: "Nanas", warna: "Kuning", biji: "tidak", harga: 9000},
    {nama: "Jeruk", warna: "Oranye", biji: "ada", harga: 8000},
    {nama: "Semangka", warna: "Hijau & Merah",biji: "ada", harga: 10000},
    {nama: "Pisang", warna: "Kuning", biji: "tidak", harga: 5000}
]

var objectBuahtanpaBiji = objectBuah.filter(function(item){
    return  item.biji == "tidak"
})

console.log("//soal 4")
console.log(objectBuahtanpaBiji)

//soal 5


function tambahDataFilm(judul, durasi, genre, tahun){

      return dataFilm.push({Judul: judul, Durasi: durasi, Genre: genre, Tahun:tahun})
  
}

var dataFilm = []
tambahDataFilm("LOTR", "2 jam", "action", "1999")
tambahDataFilm("avenger", "2 jam", "action", "2019")
tambahDataFilm("spiderman", "2 jam", "action", "2004")
tambahDataFilm("juon", "2 jam", "horror", "2004")

console.log("//soal 5")
console.log(dataFilm)