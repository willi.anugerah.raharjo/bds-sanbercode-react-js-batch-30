
//soal 1
console.log("//soal 1")
    var i = 2;
    console.log("LOOPING PERTAMA")
    while( i < 21){
        console.log(i + " - I love coding")
    i += 2;
    }

    var i = 20;
    console.log("LOOPING KEDUA")
    while( i > 1){
        console.log(i + " - I will become a frontend developer")
    i -= 2;
    }

//soal 2
console.log("//soal 2")
	for (var i = 1; i <= 20; i++) {

		if (i%2 == 1) {
			if (i%3 == 0) {
				console.log(i+ " - I Love Coding");
			} else {
				console.log(i+ " - Santai");
			}
		} else {
			console.log(i+ " - Berkualitas");
		}
	}

    //soal 3
	console.log("//Soal 3")
	for (var i = 1; i <= 7; i++) {
		for (j = 1; j <= i; j++) {
            console.log("#")
		}
		console.log("")
	}

    //soal 4
    var kalimat=["aku", "saya", "sangat", "sangat", "senang", "belajar", "javascript"]
    kalimat.splice(0,1);
    kalimat.splice(1,1)
    console.log("//soal 4")
    console.log(kalimat)

    //soal 5
    var sayuran=[]

    sayuran.splice(0, "Kangkung", "Bayam", "Buncis", "Kubis", "Timun", "Seledri", "Tauge")
    console.log("//soal 5")
    sayuran.sort()
    console.log(sayuran)