var filterBooksPromise = require('./promise2.js')
 
// Lanjutkan code untuk menjalankan function filterBookPromise
// function execute(color, amountPage){
//     filterBooksPromise(color, amountPage).then(function(check){
//         if (check){
//             console.log(check)
            
//         }
//       }).catch(function(err){
//         console.log(err)
//       })
//     }
    
    async function execute(color, amountPage){
        try {
         var result = await filterBooksPromise(color, amountPage)
         console.log(result)
        } catch(err){
          console.log(err)
        }
      }

    execute(true, 40)
    execute(false, 250)
    execute(true, 30)